package br.com.senac.beautysysweb.bean;

import br.com.senac.beautysysweb.banco.CategoriaDAO;
import br.com.senac.beautysysweb.banco.ServicoDAO;
import br.com.senac.beautysysweb.entity.Categoria;
import br.com.senac.beautysysweb.entity.Servico;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ServicoBean extends Bean {

    private final ServicoDAO dao = new ServicoDAO();
    private final CategoriaDAO daoCategoria = new CategoriaDAO();

    private Servico servico = new Servico();

    private List<Servico> lista;
    private final List<Categoria> listaCategoria = daoCategoria.findAll();

    public ServicoBean() {
        this.atualizarLista();
    }

    public void novo() {
        this.servico = new Servico();
    }

    private void atualizarLista() {
        this.lista = dao.findAll();
    }

    public void salvar() {
        if (!this.isEditando()) {
            this.dao.save(servico);
            addMessageInfo("Salvo com sucesso!");
        } else {
            this.dao.update(servico);
            addMessageInfo("Alterado com sucesso!");
        }

        this.atualizarLista();

    }

    public void excluir(Servico servico) {
        try {
            dao.delete(servico.getId());
            this.atualizarLista();
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public boolean isEditando() {
        return this.servico.getId() != 0;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public List<Servico> getLista() {
        return lista;
    }

    public void setLista(List<Servico> lista) {
        this.lista = lista;
    }

    public List<Categoria> getListaCategoria() {
        return listaCategoria;
    }
    
    

}
