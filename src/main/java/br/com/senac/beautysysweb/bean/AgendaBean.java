package br.com.senac.beautysysweb.bean;

import br.com.senac.beautysysweb.banco.AtendimentoDAO;
import br.com.senac.beautysysweb.entity.Atendimento;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@ViewScoped
public class AgendaBean extends Bean {

    private ScheduleModel eventModel;

    private final AtendimentoDAO dao = new AtendimentoDAO();

    private final List<Atendimento> lista = dao.findAll();

    private ScheduleEvent event = new DefaultScheduleEvent();

    @PostConstruct
    public void init() {
        eventModel = new DefaultScheduleModel();
        for (Atendimento atendimento : lista) {
            String titulo = atendimento.getCliente().getNome() + "(" + atendimento.getServico().getDescricao() + ")";
            Date atendimentoFinal = new Date(atendimento.getDataAgendamento().getTime() + atendimento.getServico().getTempoExecucao().getTime());
            ScheduleEvent itemAgenda = new DefaultScheduleEvent(titulo, atendimento.getDataAgendamento(), atendimentoFinal, atendimento);
            eventModel.addEvent(itemAgenda);
        }
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public void addEvent(ActionEvent actionEvent) {
        if (event.getId() == null) {
            eventModel.addEvent(event);
        } else {
            eventModel.updateEvent(event);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }

    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        
        addMessageInfo("Movido");
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        addMessageInfo("ajustado");
    }

  

}
