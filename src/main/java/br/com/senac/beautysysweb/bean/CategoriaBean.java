
package br.com.senac.beautysysweb.bean;


import br.com.senac.beautysysweb.banco.CategoriaDAO;
import br.com.senac.beautysysweb.entity.Categoria;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class CategoriaBean extends Bean{
    
    
    private Categoria categoria = new Categoria(); 
    
    private List<Categoria> lista ;
    
    private final CategoriaDAO dao = new CategoriaDAO();

    public CategoriaBean() {
        this.atualizarLista();
    }
    
    public void novo(){
        this.categoria = new Categoria();
    }
    
   
    
    private void atualizarLista(){
        this.lista = dao.findAll();
    }
    
    public void salvar(){
        if(!this.isEditando()){
            this.dao.save(categoria);
            addMessageInfo("Salvo com sucesso!");
        }else{
            this.dao.update(categoria);
            addMessageInfo("Alterado com sucesso!");
        }
        
        this.atualizarLista();
        
    }
    
    public void excluir(Categoria categoria) {
        try {
            dao.delete(categoria.getId());
            this.atualizarLista();
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
    
    public boolean isEditando(){
        return this.categoria.getId() != 0;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getLista() {
        return lista;
    }

    public void setLista(List<Categoria> lista) {
        this.lista = lista;
    }
    
    
    
    
    
    
    
}
