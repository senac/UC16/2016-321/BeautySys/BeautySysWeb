package br.com.senac.beautysysweb.banco;

import br.com.senac.beautysysweb.entity.Categoria;

public class CategoriaDAO extends DAO<Categoria> {

    public CategoriaDAO() {
        super(Categoria.class);
    }

}
