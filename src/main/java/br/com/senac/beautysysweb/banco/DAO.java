package br.com.senac.beautysysweb.banco;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class DAO<T> {

    private EntityManager em;

    private final Class<T> entidade;

    public DAO(Class<T> entidade) {
        this.entidade = entidade;
    }

    public void save(T entity) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        this.em.persist(entity);
        this.em.getTransaction().commit();
        this.em.close();

    }

    public List<T> findAll() {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();

        Query query = em.createQuery("from " + entidade.getName() + " e");
        List<T> lista = query.getResultList();

        this.em.getTransaction().commit();
        this.em.close();

        return lista;

    }

    public void update(T entity) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        this.em.merge(entity);
        this.em.getTransaction().commit();
        this.em.close();
    }

    public void delete(int id) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
         T t = em.find(entidade , id);
        this.em.remove(t);
        this.em.getTransaction().commit();
        this.em.close();
    }

    public T find(int id) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        T t = em.find(entidade, id);
        this.em.getTransaction().commit();
        this.em.close();

        return t;
    }

}
