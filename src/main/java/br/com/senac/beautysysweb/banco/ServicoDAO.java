
package br.com.senac.beautysysweb.banco;

import br.com.senac.beautysysweb.entity.Servico;

public class ServicoDAO extends DAO<Servico>{
    
    public ServicoDAO() {
        super(Servico.class);
    }
    
}
