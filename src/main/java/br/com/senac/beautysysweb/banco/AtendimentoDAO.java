package br.com.senac.beautysysweb.banco;

import br.com.senac.beautysysweb.entity.Atendimento;

public class AtendimentoDAO extends DAO<Atendimento> {

    public AtendimentoDAO() {
        super(Atendimento.class);
    }

}
