package br.com.senac.beautysysweb.banco;

import br.com.senac.beautysysweb.entity.Cliente;

public class ClienteDAO extends DAO<Cliente> {

    public ClienteDAO() {
        super(Cliente.class);
    }

}
