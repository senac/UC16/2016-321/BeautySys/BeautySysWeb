package br.com.senac.beautysysweb.entity;

import javax.persistence.Entity;

@Entity
public class Cliente extends Entidade {

    private String nome;

    /* outros atributos */
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
