package br.com.senac.beautysysweb.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Servico extends Entidade {

    private String descricao;

    private double valor;

    @Temporal(TemporalType.TIME)
    private Date tempoExecucao;

    @ManyToOne
    private Categoria categoria;

    public Servico() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getTempoExecucao() {
        return tempoExecucao;
    }

    public void setTempoExecucao(Date tempoExecucao) {
        this.tempoExecucao = tempoExecucao;
    }

    

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
