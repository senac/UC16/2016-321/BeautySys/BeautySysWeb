package br.com.senac.beautysysweb.entity;

import javax.persistence.Entity;

@Entity
public class Categoria extends Entidade {

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
