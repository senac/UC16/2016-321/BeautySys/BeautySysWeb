package br.com.senac.beautysysweb.converter;

import br.com.senac.beautysysweb.banco.CategoriaDAO;
import br.com.senac.beautysysweb.entity.Categoria;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("categoriaConverter")
public class CategoriaConverter implements Converter {

    private final CategoriaDAO dao = new CategoriaDAO();

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        Integer id = null;

        try {
            id = Integer.valueOf(value);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();

        }

        if (value != null) {
            return dao.find(id);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object value) {
        if (value != null && !value.equals("")) {
            Categoria categoria = (Categoria) value;
            return String.valueOf(categoria.getId());
        }
        return null;
    }

}
