INSERT INTO Categoria (id , descricao) VALUES( 1 ,'Cabelos');
INSERT INTO Categoria (id , descricao) VALUES( 2 , 'Estética');
INSERT INTO Categoria (id , descricao) VALUES( 3 ,'Depilação');
INSERT INTO Categoria (id , descricao) VALUES( 4, 'Unhas');


INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(1,'Corte Feminino', '02:00' , 40, 1);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(2,'Lavar e Secar','01:00' , 28, 1);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(3,'Luzes','03:00' ,320, 1);

INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(4,'Botox','01:00' , 250, 2);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(5,'Lipo Enzimática Detox','01:00' , 150, 2);

INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(6,'Depilacao axila','00:30' , 20, 3);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(7,'Virilha contorno','00:30' , 28, 3);


INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(8,'Retirada unhas posticas','00:30', 25, 4);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(9,'Unhas Postiças','00:30' , 20, 4);
INSERT INTO Servico (id ,descricao,tempoExecucao, valor, categoria_id) VALUES(10,'Pintura','00:30' , 15, 4);

INSERT INTO Cliente (id ,nome) VALUES( 1 , 'Maria da Silva');


INSERT INTO Atendimento (id ,cliente_id, dataAgendamento, servico_id) VALUES(1 ,1 ,NOW(), 1);

commit ;


